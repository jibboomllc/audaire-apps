package com.audaire.reader;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by tom on 8/1/17.
 */

public class LogArrayAdapter extends ArrayAdapter<JSONObject> {

    private final Context context;
    private final JSONObject[] values;

    public LogArrayAdapter(Context context, JSONObject[] values) {
        super(context, R.layout.list_log, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_log, parent, false);

        TextView markerView = (TextView) rowView.findViewById(R.id.marker);
        TextView summaryView = (TextView) rowView.findViewById(R.id.summary);
        TextView timeTextView = (TextView) rowView.findViewById(R.id.disposal_date_time);
        TextView detailsTextView = (TextView) rowView.findViewById(R.id.details);
        final float scale = getContext().getResources().getDisplayMetrics().density;
        final int dp40 = (int) (40 * scale + 0.5f);
        try {
            ShapeDrawable background = new ShapeDrawable();
            background.setShape(new OvalShape());
            background.setIntrinsicHeight(dp40);
            background.setIntrinsicWidth(dp40);

            switch (values[position].getString("class")) {
                case "event-important":
                    background.getPaint().setColor(Color.parseColor("#ad2121"));
                    break;
                case "event-info":
                    background.getPaint().setColor(Color.parseColor("#1e90ff"));
                    break;
                case "event-warning":
                    background.getPaint().setColor(Color.parseColor("#e3bc08"));
                    break;
                case "event-inverse":
                    background.getPaint().setColor(Color.parseColor("#1b1b1b"));
                    break;
                case "event-success":
                    background.getPaint().setColor(Color.parseColor("#006400"));
                    break;
                case "event-special":
                    background.getPaint().setColor(Color.parseColor("#800080"));
                    break;
                default:
                    background.getPaint().setColor(Color.parseColor("#000000"));
                    break;
            }
            markerView.setBackground(background);
            markerView.setText((values[position].getString("title")).substring(0,1));
            markerView.setTextColor(Color.parseColor("#ffffff"));
            summaryView.setText(values[position].getString("title"));
            summaryView.setMaxLines(1);
            summaryView.setEllipsize(TextUtils.TruncateAt.END);
            timeTextView.setText(getDate(values[position].getLong("start")));
            detailsTextView.setText(values[position].getString("title"));

            // do event-specific formatting
            JSONObject metaData = new JSONObject(values[position].getString("data"));
            //Log.i("getView", values[position].getString("type"));
            if (values[position].getString("type").equals("usage")) {
                background.getPaint().setColor(Color.parseColor("#" + metaData.getString("background_color")));
                markerView.setTextColor(Color.parseColor("#" + metaData.getString("font_color")));
                if (metaData.getString("background_color").equals("ffffff")) {
                    background.getPaint().setColor(Color.parseColor("#f4f4f4"));
                }
            }

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView detailsTextView = (TextView) v.findViewById(R.id.details);

                    Toast.makeText(getContext(), detailsTextView.getText(), Toast.LENGTH_LONG)
                            .show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowView;
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("yyyy-MM-dd HH:mm:ss", cal).toString();
        return date;
    }
}