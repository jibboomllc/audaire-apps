/*
 * Copyright (C) Audaire Health Inc
 */
package com.audaire.reader;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static com.audaire.reader.HomeFragment.API_URL;


public class LogFragment extends android.support.v4.app.Fragment {

    private ListView listView;

    private SharedPreferences settings;
    private static final String TAG = "LogFragment";
    private SwipeRefreshLayout swipeContainer;

    public LogFragment() {
    }

    public static LogFragment newInstance() {
        LogFragment fragment = new LogFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_log, container, false);

        listView = (ListView) rootView.findViewById(R.id.listview);

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new RetrieveLogTask().execute();
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        settings = PreferenceManager.getDefaultSharedPreferences(getContext());
    }


    @Override
    public void onResume() {
        super.onResume();
        if (settings != null) { // Make sure that the UI has been built
            new RetrieveLogTask().execute();
        }
    }


    class RetrieveLogTask extends AsyncTask<Void, Void, String> {

        private Exception exception;

        protected void onPreExecute() {
        }

        protected String doInBackground(Void... urls) {
            int responseCode = 0;
            String authToken = settings.getString("authToken", "");
            String userId = settings.getString("userId", "");
            if (authToken.length() > 0 && userId.length() > 0) {
                try {
                    Long fromTs = System.currentTimeMillis() - ((90 * 24 * 60 * 60) * 1000l);
                    Long toTs = System.currentTimeMillis() + ((24 * 60 * 60) * 1000l);
                    URL url = new URL(API_URL + "events/patient_id/" + userId + "?&from=" + fromTs.toString() + "&to=" + toTs.toString() + "");
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setSSLSocketFactory(new AHSSLSocketFactory(urlConnection.getSSLSocketFactory()));
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setRequestProperty("X-Authorization", authToken);
                    try {
                        responseCode = urlConnection.getResponseCode();

                        if (responseCode == 200) {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                            StringBuilder stringBuilder = new StringBuilder();
                            String line;
                            while ((line = bufferedReader.readLine()) != null) {
                                stringBuilder.append(line).append("\n");
                            }
                            bufferedReader.close();
                            //Log.i("RetrieveLogTask", stringBuilder.toString());
                            return stringBuilder.toString();
                        }
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (Exception e) {
                    Log.e("ERROR", e.getMessage(), e);
                    return null;
                }
            } else {
                return null;
            }


            switch (responseCode) {
                case 401:
                    // End activity and move to Login activity
                    Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    getActivity().finish();
                    break;
                case 403:
                    break;
                case 404:
                    break;
                default:
                    break;
            }
            return null;
        }

        protected void onPostExecute(String response) {

            if (response != null && response.length() > 0) {
                Log.i("onPostExecute", response);
                try {
                    JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();
                    JSONArray jsonArray = jsonObject.getJSONArray("result");

                    final JSONObject[] list = new JSONObject[jsonArray.length()];
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        list[i] = jsonArray.getJSONObject(i);
                    }
                    listView.setAdapter(new LogArrayAdapter(getContext(), list));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                response = "THERE WAS AN ERROR";
            }
            swipeContainer.setRefreshing(false);
        }
    }

}
