package com.audaire.reader;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParsePosition;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;

public class HomeFragment extends android.support.v4.app.Fragment {

    private LinearLayout mainContainer;
    private TextView elapsedDays;
    private TextView elapsedDaysUnits;
    private TextView elapsedHours;
    private TextView elapsedHoursUnits;
    private TextView elapsedMinutes;
    private TextView elapsedMinutesUnits;
    private TextView elapsedSeconds;
    private TextView elapsedSecondsUnits;
    private SharedPreferences settings;

    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final int RC_SURVEY = 9002;
    private static final String TAG = "HomeFragment";
    private SwipeRefreshLayout swipeContainer;

    public static final String API_URL = "https://audaire.io/api/v1/";

    private static MediaPlayer mediaPlayer;

    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (settings != null) { // Make sure that the UI has been built
            new RetrieveLastReadTask().execute();
            new RetrieveSurveysTask().execute();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        mainContainer = (LinearLayout) rootView.findViewById(R.id.mainContainer);
        elapsedDays = (TextView) rootView.findViewById(R.id.elapsedDays);
        elapsedDaysUnits = (TextView) rootView.findViewById(R.id.elapsedDaysUnits);
        elapsedHours = (TextView) rootView.findViewById(R.id.elapsedHours);
        elapsedHoursUnits = (TextView) rootView.findViewById(R.id.elapsedHoursUnits);
        elapsedMinutes = (TextView) rootView.findViewById(R.id.elapsedMinutes);
        elapsedMinutesUnits = (TextView) rootView.findViewById(R.id.elapsedMinutesUnits);
        elapsedSeconds = (TextView) rootView.findViewById(R.id.elapsedSeconds);
        elapsedSecondsUnits = (TextView) rootView.findViewById(R.id.elapsedSecondsUnits);
        settings = PreferenceManager.getDefaultSharedPreferences(getContext());

        Button scanButton = (Button) rootView.findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // launch barcode activity.
                Intent intent = new Intent(getActivity(), BarcodeCaptureActivity.class);
                intent.putExtra(BarcodeCaptureActivity.AutoFocus, true); // autoFocus.isChecked());
                intent.putExtra(BarcodeCaptureActivity.UseFlash, true); // useFlash.isChecked());

                startActivityForResult(intent, RC_BARCODE_CAPTURE);
            }
        });

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new RetrieveLastReadTask().execute();
                new RetrieveSurveysTask().execute();
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        return rootView;
    }

    /**
     * Called when an activity you launched exits, giving you the requestCode
     * you started it with, the resultCode it returned, and any additional
     * data from it.  The <var>resultCode</var> will be
     * RESULT_CANCELED if the activity explicitly returned that,
     * didn't return any result, or crashed during its operation.
     * <p/>
     * <p>You will receive this call immediately before onResume() when your
     * activity is re-starting.
     * <p/>
     *
     * @param requestCode The integer request code originally supplied to
     *                    startActivityForResult(), allowing you to identify who this
     *                    result came from.
     * @param resultCode  The integer result code returned by the child activity
     *                    through its setResult().
     * @param data        An Intent, which can return result data to the caller
     *                    (various data can be attached to Intent "extras").
     * @see #startActivityForResult
     * see #createPendingResult
     * see #setResult(int)
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    //Log.d(TAG, "Barcode read: " + barcode.displayValue);
                    new RegisterReadTask().execute(barcode.displayValue);
                } else {
                    Log.d(TAG, "No barcode captured, intent data is null");
                }
            } else {
                //statusMessage.setText(String.format(getString(R.string.barcode_error),
                //        CommonStatusCodes.getStatusCodeString(resultCode)));
            }
        } else if (requestCode == RC_SURVEY) {

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    class RetrieveLastReadTask extends AsyncTask<Void, Void, String> {

        private Exception exception;

        protected void onPreExecute() {
            elapsedDays.setText("-");
            elapsedHours.setText("-");
            elapsedMinutes.setText("-");
            elapsedSeconds.setText("-");
        }

        protected String doInBackground(Void... urls) {
            String authToken = settings.getString("authToken", "");
            String userId = settings.getString("userId", "");
            if (authToken.length() > 0 && userId.length() > 0) {
                try {
                    URL url = new URL(API_URL + "usages/patient_id/" + userId + "/last");
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setSSLSocketFactory(new AHSSLSocketFactory(urlConnection.getSSLSocketFactory()));
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setRequestProperty("X-Authorization", authToken);
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                        StringBuilder stringBuilder = new StringBuilder();
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            stringBuilder.append(line).append("\n");
                        }
                        bufferedReader.close();
                        return stringBuilder.toString();
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (Exception e) {
                    Log.e("ERROR", e.getMessage(), e);
                    return null;
                }
            } else {
                return null;
            }
        }

        protected void onPostExecute(String response) {
            //Log.i("INFO", response);

            if (response != null && response.length() > 0) {
                if (!response.startsWith("false")) {
                    try {
                        // {"disposal_time":"2017-01-12 04:27:48.440414-08"}
                        JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
                        String dtStart = object.getString("disposal_time");
                        ParsePosition pos = new ParsePosition(0);
                        SimpleDateFormat formatZ = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSSZ");
                        Date disposalDate = formatZ.parse(dtStart, pos);
                        if (disposalDate == null) {
                            SimpleDateFormat formatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSSX");
                            disposalDate = formatX.parse(dtStart, pos);
                        }
                        ElapsedTime dt = new ElapsedTime(disposalDate);
                        elapsedDays.setText(Long.toString(dt.getDays()));
                        elapsedDaysUnits.setText("Day" + ((dt.getDays() != 1) ? "s" : ""));
                        elapsedHours.setText(((dt.getHours() < 10) ? "0" : "") + dt.getHours());
                        elapsedHoursUnits.setText("Hour" + ((dt.getHours() != 1) ? "s" : ""));
                        elapsedMinutes.setText(((dt.getMinutes() < 10) ? "0" : "") + dt.getMinutes());
                        elapsedMinutesUnits.setText("Minute" + ((dt.getMinutes() != 1) ? "s" : ""));
                        elapsedSeconds.setText(((dt.getSeconds() < 10) ? "0" : "") + dt.getSeconds());
                        elapsedSecondsUnits.setText("Second" + ((dt.getSeconds() != 1) ? "s" : ""));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            swipeContainer.setRefreshing(false);
        }
    }

    class ElapsedTime {
        private Date since;
        private long days;
        private long hours;
        private long minutes;
        private long seconds;

        public ElapsedTime(Date since) {
            this.since = since;
            calculate();
        }

        public Date getSince() {
            return since;
        }

        public void setSince(Date since) {
            this.since = since;
            calculate();
        }

        public long getDays() {
            return days;
        }

        public long getHours() {
            return hours;
        }

        public long getMinutes() {
            return minutes;
        }

        public long getSeconds() {
            return seconds;
        }

        private void calculate() {
            days = 0;
            hours = 0;
            minutes = 0;
            seconds = 0;

            if (since != null) {
                Date now = new Date();
                long msPerDay = 1000 * 60 * 60 * 24;
                long msPerHour = 1000 * 60 * 60;
                long msPerMinute = 1000 * 60;

                long dT = now.getTime() - since.getTime();
                days = dT / msPerDay;
                dT -= days * msPerDay;

                hours = dT / msPerHour;
                dT -= hours * msPerHour;

                minutes = dT / msPerMinute;
                dT -= minutes * msPerMinute;

                seconds = dT / 1000;
            }
        }
    }

    class RetrieveSurveysTask extends AsyncTask<Void, Void, String> {

        private Exception exception;

        protected void onPreExecute() {
        }

        protected String doInBackground(Void... urls) {
            int responseCode = 0;
            String authToken = settings.getString("authToken", "");
            String userId = settings.getString("userId", "");
            if (authToken.length() > 0 && userId.length() > 0) {
                try {
                    URL url = new URL(API_URL + "surveys/patient_id/" + userId);
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setSSLSocketFactory(new AHSSLSocketFactory(urlConnection.getSSLSocketFactory()));
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setRequestProperty("X-Authorization", authToken);
                    try {
                        responseCode = urlConnection.getResponseCode();

                        if (responseCode == 200) {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                            StringBuilder stringBuilder = new StringBuilder();
                            String line;
                            while ((line = bufferedReader.readLine()) != null) {
                                stringBuilder.append(line).append("\n");
                            }
                            bufferedReader.close();
                            //Log.i("RetrieveLogTask", stringBuilder.toString());
                            return stringBuilder.toString();
                        }
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (Exception e) {
                    Log.e("ERROR", e.getMessage(), e);
                    return null;
                }
            } else {
                return null;
            }


            switch (responseCode) {
                case 401:
                    // End activity and move to Login activity
                    Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    getActivity().finish();
                    break;
                case 403:
                    break;
                case 404:
                    break;
                default:
                    break;
            }
            return null;
        }

        protected void onPostExecute(String response) {

            if (response != null && response.length() > 0) {
                //Log.i("onPostExecute", response);

                while (mainContainer.getChildCount() > 5 ) {
                    mainContainer.removeViewAt(5);
                }
                try {
                    JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();
                    JSONArray jsonArray = jsonObject.getJSONArray("surveys");

                    for (int i = 0; i < jsonArray.length(); ++i) {
                        final String surveyName =  jsonArray.getJSONObject(i).getString("name");
                        final String surveyId =  jsonArray.getJSONObject(i).getString("id");

                        AHButton surveyItem = new AHButton(getContext());
                        surveyItem.setText(surveyName);

                        surveyItem.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(getActivity(), SurveyActivity.class);
                                intent.putExtra(SurveyActivity.SurveyName, surveyName);
                                intent.putExtra(SurveyActivity.SurveyId, surveyId);

                                startActivityForResult(intent, RC_SURVEY);
                            }
                        });

                        mainContainer.addView(surveyItem);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                response = "THERE WAS AN ERROR";
            }
            swipeContainer.setRefreshing(false);
        }
    }

    class RegisterReadTask extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected void onPreExecute() {
        }

        protected String doInBackground(String... scannedCodes) {
            String authToken = settings.getString("authToken", "");
            if (authToken.length() > 0) {
                try {
                    URL url = new URL(API_URL + "usages/verbose");
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setSSLSocketFactory(new AHSSLSocketFactory(urlConnection.getSSLSocketFactory()));

                    WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
                    Display display = wm.getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);

                    String urlParameters = "disposal_data={"
                            + "\"type\":\"barcode\",\"platform\":\"android\""
                            + ",\"os_version\":\"" + android.os.Build.VERSION.SDK_INT + "\""
                            + ",\"os_locale\":\"" + Locale.getDefault().toLanguageTag() + "\""
                            + ",\"app_version\":\"" + BuildConfig.VERSION_NAME + "\""
                            + ",\"display_x\":\"" + size.x + "\", \"display_y\":\"" + size.y + "\""
                            // , "latitude":"xx", "longitude":"xx", "device_id":"xx"
                            + "}&tag_ids=[\"" + scannedCodes[0] + "\"]";
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                    urlConnection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
                    urlConnection.setRequestProperty("X-Authorization", authToken);
                    urlConnection.setDoOutput(true);
                    DataOutputStream dStream = new DataOutputStream(urlConnection.getOutputStream());
                    dStream.writeBytes(urlParameters);
                    dStream.flush();
                    dStream.close();

                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                        StringBuilder stringBuilder = new StringBuilder();
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            stringBuilder.append(line).append("\n");
                        }
                        bufferedReader.close();
                        return stringBuilder.toString();
                    } catch (Exception e) {
                        Log.e("ERROR", e.getMessage(), e);
                        return null;
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (Exception e) {
                    Log.e("ERROR", e.getMessage(), e);
                    return null;
                }
            } else {
                return null;
            }
        }

        protected void onPostExecute(String response) {
            Log.d("INFO", response);

            if (response != null && response.length() > 0) {
                try {
                    // {"reads_saved":1,"details":[{"tag_id":"a59d7df7cce6af5037759180","result":"DEMO"}]}
                    JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();

                    JSONArray detailsArray = jsonObject.getJSONArray("details");
                    String result = new String();
                    String tagId = new String();
                    String message = new String();
                    for (int i = 0; i < detailsArray.length(); ++i) {
                        result = detailsArray.getJSONObject(i).getString("result");
                        tagId = detailsArray.getJSONObject(i).getString("tag_id");

                        switch (result) {
                            case "OK":
                                message = "Successfully scanned label: " + tagId;
                                break;
                            case "DEMO":
                                message = "Successfully scanned demo label: " + tagId;
                                break;
                            case "BAD":
                                message = "Code: " + tagId + " not recognized. Please make sure that you are scanning the code on the small Audaire label.";
                                break;
                            case "USED":
                                message = "Rescanned label: " + tagId + ". Originally scanned: " + detailsArray.getJSONObject(i).getString("disposal_time");
                                break;
                            case "UNREG":
                                message = "Label: " + tagId +" does not appear to be registered with Audaire";
                                break;
                            case "ERROR":
                                message = "Error saving label: " + tagId;
                                break;
                            default:
                                message = "Unknown status: " + tagId;
                                break;
                        };
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }

                    if (jsonObject.getInt("reads_saved") > 0) {
                        mediaPlayer = MediaPlayer.create(getActivity(), R.raw.success);
                        mediaPlayer.start();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                response = "THERE WAS AN ERROR";
            }
        }
    }
}
