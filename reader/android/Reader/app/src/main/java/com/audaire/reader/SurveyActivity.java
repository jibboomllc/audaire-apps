package com.audaire.reader;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Calendar;

import javax.net.ssl.HttpsURLConnection;

import static com.audaire.reader.HomeFragment.API_URL;

public class SurveyActivity extends AppCompatActivity {
    // constants used to pass extra data in the intent
    public static final String SurveyName = "SurveyName";
    public static final String SurveyId = "SurveyId";

    private String surveyId = null;
    private String responseId = null;
    private String questionId = null;
    private String answer = null;

    private SharedPreferences settings;

    // UI references.
    private LinearLayout mainContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_survey);
        mainContainer = (LinearLayout) findViewById(R.id.mainContainer);
        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        // read parameters from the intent used to launch the activity.
        setTitle(getIntent().getStringExtra(SurveyName));
        surveyId = getIntent().getStringExtra(SurveyId);

        new RegisterResponseTask().execute();
    }

    public void cancelConfirmed() {
        new RemoveResponseTask().execute();
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        cancelConfirmed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mainContainer.getContext());
        builder.setMessage("If you cancel now, all the information you just entered will be deleted.")
                .setPositiveButton("Please delete the information.", dialogClickListener)
                .setNegativeButton("I'll finish entering information.", dialogClickListener)
                .show();
    }

    public void buildScreen(JSONObject jsonObject) {

        if(jsonObject != null && jsonObject.length() > 0) {
            mainContainer.removeAllViews();
            try {
                questionId = jsonObject.getString("question_id");

                TextView question = new TextView(getApplicationContext());
                question.setGravity(Gravity.CENTER);
                question.setPadding(20, 20, 20, 40);
                question.setTextSize(18.0f);
                question.setText(jsonObject.getString("question"));
                mainContainer.addView(question);

                WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(size.x / 20, 40, size.x / 20, 40);

                AHButton nextButton = new AHButton(getApplicationContext());
                nextButton.setText(jsonObject.getString("next_button_text"));

                final String questionTypeId = jsonObject.getString("question_type_id");
                switch (questionTypeId) {
                    case "mc":
                        JSONArray choicesArray = jsonObject.getJSONArray("choices");

                        for (int i = 0; i < choicesArray.length(); ++i) {
                            AHButton choiceButton = new AHButton(getApplicationContext());
                            choiceButton.setText(choicesArray.getJSONObject(i).getString("content"));
                            mainContainer.addView(choiceButton);

                            choiceButton.setOnClickListener(choiceButton.new AHOnClickListener(choicesArray.getJSONObject(i).getString("id")) {
                                @Override
                                public void onClick(View v) {
                                    answer = this.value;
                                    new AnswerQuestionTask().execute();
                                }
                            });
                        }
                        break;
                    case "oe": //FIXME Remove
                    case "dp":
                    case "df":
                        final TextView dateText = new TextView(this);
                        dateText.setText("YYYY-MM-DD");
                        dateText.setLayoutParams(layoutParams);
                        mainContainer.addView(dateText);

                        dateText.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                Calendar currentDate = Calendar.getInstance();
                                int currentYear = currentDate.get(Calendar.YEAR);
                                int currentMonth = currentDate.get(Calendar.MONTH);
                                int currentDay = currentDate.get(Calendar.DAY_OF_MONTH);

                                DatePickerDialog datePickerDialog;
                                datePickerDialog = new DatePickerDialog(mainContainer.getContext(), new DatePickerDialog.OnDateSetListener() {
                                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                                        selectedmonth = selectedmonth + 1;
                                        dateText.setText(selectedyear + "-" + (selectedmonth < 10 ? "0" : "")
                                                + selectedmonth + "-" + (selectedday < 10 ? "0" : "") + selectedday);
                                    }
                                }, currentYear, currentMonth, currentDay);
                                if (questionTypeId.equals("df")) {
                                    datePickerDialog.getDatePicker().setMinDate(currentDate.getTimeInMillis());
                                } else {
                                    datePickerDialog.getDatePicker().setMaxDate(currentDate.getTimeInMillis());
                                }
                                datePickerDialog.setTitle("Select Date");
                                datePickerDialog.show();
                            }
                        });
                        mainContainer.addView(nextButton);

                        nextButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //answer = editText.getText().toString();
                                new AnswerQuestionTask().execute();
                            }
                        });
                        break;
                    case "oeFIXME": //FIXME
                        final EditText editText = new EditText(this);
                        editText.setLayoutParams(layoutParams);
                        mainContainer.addView(editText);

                        mainContainer.addView(nextButton);

                        nextButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                answer = editText.getText().toString();
                                new AnswerQuestionTask().execute();
                            }
                        });

                        editText.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                        break;
                    case "st":
                        mainContainer.addView(nextButton);

                        nextButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                answer = null;
                                new AnswerQuestionTask().execute();
                            }
                        });
                        break;
                    default:
                        TextView message = new TextView(getApplicationContext());
                        message.setText("[Unsupported question_type_id = " + questionTypeId + "]");
                        mainContainer.addView(message);
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            super.onBackPressed();
        }
    }

    //////////////////////////////////////////////////////
    //
    // RegisterResponseTask
    //
    public class RegisterResponseTask extends AsyncTask<Void, Void, String> {
        private Exception exception;

        protected String doInBackground(Void... urls) {
            int responseCode = 0;
            String authToken = settings.getString("authToken", "");
            String userId = settings.getString("userId", "");
            if (authToken.length() > 0 && userId.length() > 0) {
                try {
                    URL url = new URL(API_URL + "surveys/" + surveyId + "/responses");
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setSSLSocketFactory(new AHSSLSocketFactory(urlConnection.getSSLSocketFactory()));
                    String urlParameters = "respondent_id=" + userId + "&survey_id=" + surveyId;
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                    urlConnection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
                    urlConnection.setRequestProperty("X-Authorization", authToken);
                    urlConnection.setDoOutput(true);
                    DataOutputStream dStream = new DataOutputStream(urlConnection.getOutputStream());
                    dStream.writeBytes(urlParameters);
                    dStream.flush();
                    dStream.close();

                    try {
                        responseCode = urlConnection.getResponseCode();

                        if (responseCode == 200) {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                            StringBuilder stringBuilder = new StringBuilder();
                            String line;
                            while ((line = bufferedReader.readLine()) != null) {
                                stringBuilder.append(line).append("\n");
                            }
                            bufferedReader.close();
                            //Log.i("RetrieveLogTask", stringBuilder.toString());
                            return stringBuilder.toString();
                        }
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (Exception e) {
                    Log.e("ERROR", e.getMessage(), e);
                    return null;
                }
            } else {
                return null;
            }

            switch (responseCode) {
                case 401:
                    // End activity and move to Login activity
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    break;
                case 403:
                    break;
                case 404:
                    break;
                default:
                    break;
            }
            return null;
        }

        protected void onPostExecute(String response) {

            if (response != null && response.length() > 0) {
                //Log.i("onPostExecute", response);

                try {
                    JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();
                    responseId = jsonObject.getString("id");
                    new NextQuestionTask().execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                response = "THERE WAS AN ERROR";
            }
        }
    }


    //////////////////////////////////////////////////////
    //
    // NextQuestionTask
    //
    public class NextQuestionTask extends AsyncTask<Void, Void, String> {
        private Exception exception;

        protected String doInBackground(Void... urls) {
            int responseCode = 0;
            String authToken = settings.getString("authToken", "");
            String userId = settings.getString("userId", "");
            if (authToken.length() > 0 && userId.length() > 0) {
                try {
                    URL url = new URL(API_URL + "surveys/" + surveyId + "/responses/" + responseId + "/next_question");
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setSSLSocketFactory(new AHSSLSocketFactory(urlConnection.getSSLSocketFactory()));
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setRequestProperty("X-Authorization", authToken);
                    try {
                        responseCode = urlConnection.getResponseCode();

                        if (responseCode == 200) {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                            StringBuilder stringBuilder = new StringBuilder();
                            String line;
                            while ((line = bufferedReader.readLine()) != null) {
                                stringBuilder.append(line).append("\n");
                            }
                            bufferedReader.close();
                            //Log.i("RetrieveLogTask", stringBuilder.toString());
                            return stringBuilder.toString();
                        }
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (Exception e) {
                    Log.e("ERROR", e.getMessage(), e);
                    return null;
                }
            } else {
                return null;
            }


            switch (responseCode) {
                case 401:
                    // End activity and move to Login activity
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    break;
                case 403:
                    break;
                case 404:
                    break;
                default:
                    break;
            }
            return null;
        }

        protected void onPostExecute(String response) {
            if (response != null && response.length() > 0) {
                if (response.startsWith("[]")) {
                    buildScreen(null);
                } else {
                    Log.d("onPostExecute", response);
                    try {
                        JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();
                        buildScreen(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                response = "THERE WAS AN ERROR";
            }
        }
    }


    //////////////////////////////////////////////////////
    //
    // AnswerQuestionTask
    //
    public class AnswerQuestionTask extends AsyncTask<Void, Void, String> {
        private Exception exception;

        protected String doInBackground(Void... urls) {
            int responseCode = 0;
            String authToken = settings.getString("authToken", "");
            String userId = settings.getString("userId", "");
            if (authToken.length() > 0 && userId.length() > 0) {
                try {
                    URL url = new URL(API_URL + "surveys/" + surveyId + "/responses/" + responseId + "/next_question");
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setSSLSocketFactory(new AHSSLSocketFactory(urlConnection.getSSLSocketFactory()));
                    String urlParameters = "question_id=" + questionId + "&answer=" + answer;
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                    urlConnection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
                    urlConnection.setRequestProperty("X-Authorization", authToken);
                    urlConnection.setDoOutput(true);
                    DataOutputStream dStream = new DataOutputStream(urlConnection.getOutputStream());
                    dStream.writeBytes(urlParameters);
                    dStream.flush();
                    dStream.close();

                    try {
                        responseCode = urlConnection.getResponseCode();

                        if (responseCode == 200) {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                            StringBuilder stringBuilder = new StringBuilder();
                            String line;
                            while ((line = bufferedReader.readLine()) != null) {
                                stringBuilder.append(line).append("\n");
                            }
                            bufferedReader.close();
                            //Log.i("RetrieveLogTask", stringBuilder.toString());
                            return stringBuilder.toString();
                        }
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (Exception e) {
                    Log.e("ERROR", e.getMessage(), e);
                    return null;
                }
            } else {
                return null;
            }

            switch (responseCode) {
                case 401:
                    // End activity and move to Login activity
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    break;
                case 403:
                    break;
                case 404:
                    break;
                default:
                    break;
            }
            return null;
        }

        protected void onPostExecute(String response) {

            if (response != null && response.length() > 0) {
                //Log.i("onPostExecute", response);

                try {
                    JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();
                    //responseId = jsonObject.getString("id");
                    new NextQuestionTask().execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                response = "THERE WAS AN ERROR";
            }
        }
    }

    //////////////////////////////////////////////////////
    //
    // RemoveResponseTask
    //
    public class RemoveResponseTask extends AsyncTask<Void, Void, String> {
        private Exception exception;

        protected String doInBackground(Void... urls) {
            int responseCode = 0;
            String authToken = settings.getString("authToken", "");
            String userId = settings.getString("userId", "");
            if (authToken.length() > 0 && userId.length() > 0) {
                try {
                    URL url = new URL(API_URL + "surveys/" + surveyId + "/responses/" + responseId);
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setSSLSocketFactory(new AHSSLSocketFactory(urlConnection.getSSLSocketFactory()));
                    urlConnection.setRequestMethod("DELETE");
                    urlConnection.setRequestProperty("X-Authorization", authToken);

                    try {
                        responseCode = urlConnection.getResponseCode();

                        if (responseCode == 200) {
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                            StringBuilder stringBuilder = new StringBuilder();
                            String line;
                            while ((line = bufferedReader.readLine()) != null) {
                                stringBuilder.append(line).append("\n");
                            }
                            bufferedReader.close();
                            //Log.i("RetrieveLogTask", stringBuilder.toString());
                            return stringBuilder.toString();
                        }
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (Exception e) {
                    Log.e("ERROR", e.getMessage(), e);
                    return null;
                }
            } else {
                return null;
            }

            switch (responseCode) {
                case 401:
                    // End activity and move to Login activity
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    break;
                case 403:
                    break;
                case 404:
                    break;
                default:
                    break;
            }
            return null;
        }

        protected void onPostExecute(String response) {

            if (response != null && response.length() > 0) {
                //Log.i("onPostExecute", response);
            } else {
                response = "THERE WAS AN ERROR";
            }
        }
    }

}

