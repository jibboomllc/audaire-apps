package com.audaire.reader;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v4.preference.PreferenceFragment;
import android.util.Log;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import static com.audaire.reader.HomeFragment.API_URL;
//import static com.audaire.reader.R.xml.settings;

/**
 * Created by tom on 1/14/17.
 */

public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        String userName = settings.getString("firstName", "") + " " + settings.getString("lastName", "");

        addPreferencesFromResource(R.xml.settings);

        findPreference("versionName").setSummary(BuildConfig.VERSION_NAME);
        findPreference("versionCode").setSummary(String.valueOf(BuildConfig.VERSION_CODE));
        findPreference("loggedIn").setSummary(userName);
        findPreference("loggedIn").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        // End activity and move to Login activity
                        Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        getActivity().finish();
                        return true;
                    }
                });

        findPreference("readerSerialNumber").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
            @Override
            public boolean onPreferenceClick(Preference p){
                new RetrieveLastPingTask().execute();
                return false;
            }
        });

        findPreference("readerLastCheck").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(){
            @Override
            public boolean onPreferenceClick(Preference p){
                new RetrieveLastPingTask().execute();
                return false;
            }
        });

        new RetrieveLastPingTask().execute();
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            // For all other preferences, set the summary to the value's
            // simple string representation.
            preference.setSummary(stringValue);

            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }


    class RetrieveLastPingTask extends AsyncTask<Void, Void, String> {

        private Exception exception;

        protected void onPreExecute() {
            findPreference("readerSerialNumber").setSummary("-");
            findPreference("readerLastCheck").setSummary("-");
        }

        protected String doInBackground(Void... urls) {
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
            String authToken = settings.getString("authToken", "");
            String userId = settings.getString("userId", "");

            if (authToken.length() > 0 && userId.length() > 0) {
                try {
                    URL url = new URL(API_URL + "ping/patient_id/" + userId);
                    HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                    urlConnection.setSSLSocketFactory(new AHSSLSocketFactory(urlConnection.getSSLSocketFactory()));
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setRequestProperty("X-Authorization", authToken);
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                        StringBuilder stringBuilder = new StringBuilder();
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            stringBuilder.append(line).append("\n");
                        }
                        bufferedReader.close();
                        Log.i("INFO", stringBuilder.toString());
                        return stringBuilder.toString();
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (Exception e) {
                    Log.e("ERROR", e.getMessage(), e);
                    return null;
                }
            } else {
                return null;
            }
        }

        protected void onPostExecute(String response) {
            //Log.i("INFO", response);

            if (response != null && response.length() > 0) {
                try {
                    // {"reader_found":1,"data":{"last_ping":"2017-01-10 11:29:05.929223-08"}}
                    JSONObject object = (JSONObject) new JSONTokener(response).nextValue();
                    if (object.getInt("reader_found") == 1 && !object.isNull("data")) {
                        if (!object.getJSONObject("data").isNull("serial_number")) {
                            findPreference("readerSerialNumber").setSummary(object.getJSONObject("data").getString("serial_number"));
                        }
                        if (!object.getJSONObject("data").isNull("last_ping")) {
                            String dtStart = object.getJSONObject("data").getString("last_ping");
                            ParsePosition pos = new ParsePosition(0);
                            SimpleDateFormat formatZ = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSSZ");
                            Date now = new Date();
                            Date pingDate = formatZ.parse(dtStart, pos);
                            if (pingDate == null) {
                                SimpleDateFormat formatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSSX");
                                pingDate = formatX.parse(dtStart, pos);
                            }
                            long elapsedMinutes = (now.getTime() - pingDate.getTime()) / 1000 / 60;
                            if (elapsedMinutes < 0) {
                                elapsedMinutes = 0;
                            }
                            if (elapsedMinutes <= 60) {
                                findPreference("readerLastCheck").setSummary(elapsedMinutes + " min" + ((elapsedMinutes != 1) ? "s" : ""));
                            } else {
                                findPreference("readerLastCheck").setSummary("60+ mins");
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

}
