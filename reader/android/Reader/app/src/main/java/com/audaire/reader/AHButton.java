package com.audaire.reader;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

/**
 * Created by tom on 3/13/18.
 */

//public class AHButton extends Button {
public class AHButton extends android.support.v7.widget.AppCompatButton {

    public AHButton(Context context) {
        super(context);

        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(size.x/10, 40, size.x/10, 40);
        this.setLayoutParams(layoutParams);
        this.setTextColor(Color.parseColor("#007aff"));
        this.setTextColor(ContextCompat.getColor(getContext(), R.color.colorControl));
        this.setBackgroundResource(R.drawable.rounded_corners_rectangle);
    }

    public class AHOnClickListener implements OnClickListener
    {
        String value = null;

        public AHOnClickListener(String value) {
            this.value = value;
        }

        @Override
        public void onClick(View v)
        {
            //read your lovely variable
        }

    }
}

