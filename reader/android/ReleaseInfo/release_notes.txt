---------------------------------------------------------------------
- Update versionCode and versionName in build.gradle (Module: app)
- Verify API_URL in HomeFragment.java 
---------------------------------------------------------------------
0m1
Masterkey j1bb00m
Keystore: /Users/tom/Documents/development/audaire/audaire-apps/reader/android/app-keystore.jks smartgrid#1
Key-alias: app-key smartgrid#1

https://play.google.com/apps/publish/?dev_acc=05317280901394703406#AppListPlace
---------------------------------------------------------------------

Description:
This app gives you the ability to log disposals via a barcode as well as view all disposals that you have made.

Reviewer Notes:
hem@foo.com / b4r

9/29/2017 - 2.0.1 New
- Login to your Audaire account
- Barcode scanning support
- Disposals log

12/14/2017 - 2.0.4 Bug fix
- Fixed issue with log timestamps (introduced when API changed to address browser issues)
