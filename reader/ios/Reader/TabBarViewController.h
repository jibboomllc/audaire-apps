//
//  TabBarViewController.h
//  Reader
//
//  Created by Tom Giesberg on 1/7/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarViewController : UITabBarController

@end
