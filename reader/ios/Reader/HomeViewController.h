//
//  HomeViewController.h
//  Reader
//
//  Created by Tom Giesberg on 12/19/16.
//  Copyright © 2016 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AHElapsedTimeView.h"
#import "AHButton.h"

@interface HomeViewController : UIViewController {
    IBOutlet UIScrollView *scrollView;

    UIStackView *stackView;
    AHElapsedTimeView *elapsedTimeView;
    UILabel *elapsedTimeLabel;
    AHButton *scanBarcodeButton;
    id surveysObject;
}

@end

