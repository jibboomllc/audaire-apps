//
//  SurveyViewController.m
//  Reader
//
//  Created by Tom Giesberg on 12/30/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import <AVFoundation/AVFoundation.h>

#import "SurveyViewController.h"
#import "AppConstants.h"
#import "AHButton.h"

@interface SurveyViewController ()

@end

@implementation SurveyViewController

@synthesize surveyId;
@synthesize surveyTitle;
@synthesize responseId;
@synthesize questionObject;
@synthesize textView;
@synthesize datePicker;

- (void)viewDidLoad {
    [super viewDidLoad];
    [navBar.topItem setTitle:surveyTitle];
    [self registerSurveyResponse];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self buildScreen];
}

- (void)viewWillAppear:(BOOL)animated {
}

- (void)viewWillDisappear:(BOOL)animated {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelToHome:(UIStoryboardSegue *) segue {
    if (responseId.length > 0) {
        
#if (TARGET_IPHONE_SIMULATOR)
        [self removeSurveyResponse];
#else
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Warning"
                                     message:@"If you cancel now, all the information you just entered will be deleted."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Please delete the information."
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        [self removeSurveyResponse];
                                        NSLog(@"Yes pushed");
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"I'll finish entering information."
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                       NSLog(@"No pushed");
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
#endif
    }
}

- (void)registerSurveyResponse {
    NSString *authToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"authToken"];
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
    if (userId && userId.length > 0) {
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [manager.requestSerializer setValue:@"application/form-data; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        NSString *url = [NSString stringWithFormat: @"%@surveys/%@/responses", baseURLString, surveyId];
        
        NSDictionary *parametersDictionary = @{
                                               @"survey_id": surveyId
                                               , @"respondent_id": userId
                                               };
        [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (responseObject != Nil) {
                responseId = [responseObject valueForKeyPath:@"id"];
                NSLog(@"responseId: %@", responseId);
                [self getNextQuestion];
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error: %@", error);
        }];
    } else {
        NSLog(@"Error: Not logged in");
    }

    return;
}

- (void)getNextQuestion {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *authToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"authToken"];
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
    if (userId && userId.length > 0) {
        
        NSString *url = [NSString stringWithFormat: @"%@surveys/%@/responses/%@/next_question", baseURLString, surveyId, responseId];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            if (responseObject != Nil) {
                // NSLog(@"JSON: %@", responseObject);
                questionObject = responseObject;
                [self buildScreen];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    } else {
        NSLog(@"Error: Not logged in");
    }
}

- (void)answerQuestion:(NSString *) answer {
    NSString *authToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"authToken"];
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
    if (userId && userId.length > 0) {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [manager.requestSerializer setValue:@"application/form-data; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        NSString *url = [NSString stringWithFormat: @"%@surveys/%@/responses/%@/next_question", baseURLString, surveyId, responseId];
        
        NSDictionary *parametersDictionary = @{
                                               @"question_id": [questionObject valueForKeyPath:@"question_id"]
                                               , @"answer": answer
                                               };
        [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (responseObject != Nil) {
                NSLog(@"responseObject: %@", responseObject);
                [self getNextQuestion];
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error: %@", error);
        }];
    } else {
        NSLog(@"Error: Not logged in");
    }
    
    return;
}

- (void)buildScreen {
    if  (questionObject && [questionObject count] > 0) {
        // clean up any old views
        [[scrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

        // build stack view
        UIStackView *stackView = [[UIStackView alloc] init];
        stackView.axis = UILayoutConstraintAxisVertical;
        stackView.distribution = UIStackViewDistributionEqualSpacing;
        stackView.alignment = UIStackViewAlignmentCenter;
        stackView.spacing = 30;
        stackView.layoutMargins = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
        stackView.layoutMarginsRelativeArrangement = true;
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        [scrollView addSubview:stackView];
        
        [stackView.leadingAnchor constraintEqualToAnchor:scrollView.leadingAnchor].active = true;
        [stackView.trailingAnchor constraintEqualToAnchor:scrollView.trailingAnchor].active = true;
        [stackView.topAnchor constraintEqualToAnchor:scrollView.topAnchor].active = true;
        [stackView.bottomAnchor constraintEqualToAnchor:scrollView.bottomAnchor].active = true;
        [stackView.widthAnchor constraintEqualToAnchor:scrollView.widthAnchor].active = true;

        // build question label
        UILabel *questionLabel = [[UILabel alloc]init];
        questionLabel.numberOfLines = 0;
        questionLabel.textAlignment = NSTextAlignmentCenter;
        questionLabel.text = [questionObject valueForKeyPath:@"question"];
        [stackView addArrangedSubview:questionLabel];

        // build question answer controls
        NSString *questionTypeId = [questionObject valueForKeyPath:@"question_type_id"];
        NSString *nextButtonText = [questionObject valueForKeyPath:@"next_button_text"];
  
        if ([questionTypeId isEqualToString:@"mc"]) { // multiple choice
            //NSLog(@"JSON: %@", questionObject);
            NSMutableArray *choices = [questionObject mutableArrayValueForKey:@"choices"];
            for (int i = 0; i < [choices count]; i++) {
                AHButton *button = [AHButton buttonWithType:UIButtonTypeSystem];
                [button addTarget:self
                           action:@selector(makeChoice:)
                 forControlEvents:UIControlEventTouchUpInside];
                [button setTitle:[choices[i] valueForKeyPath:@"content"] forState:UIControlStateNormal];
                [button setUserData:[choices[i] valueForKeyPath:@"id"]];
                [stackView addArrangedSubview:button];
            }
        } else if ([questionTypeId isEqualToString:@"dp"]) { // date past
            self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
            [self.datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
            [self.datePicker setMaximumDate:[NSDate date]];
            [self.datePicker setMinuteInterval:15];
            [stackView addArrangedSubview:self.datePicker];
            
            AHButton *button = [AHButton buttonWithType:UIButtonTypeSystem];
            [button addTarget:self
                       action:@selector(dateEntered:)
             forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:nextButtonText forState:UIControlStateNormal];
            [stackView addArrangedSubview:button];
        
        } else if ([questionTypeId isEqualToString:@"df"]) { // date future
            self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
            [self.datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
            [self.datePicker setMinimumDate:[NSDate date]];
            [self.datePicker setMinuteInterval:15];
            [stackView addArrangedSubview:self.datePicker];
            
            AHButton *button = [AHButton buttonWithType:UIButtonTypeSystem];
            [button addTarget:self
                       action:@selector(dateEntered:)
             forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:nextButtonText forState:UIControlStateNormal];
            [stackView addArrangedSubview:button];

        } else if ([questionTypeId isEqualToString:@"oe"]) { // open ended
            self.textView = [[UITextView  alloc]init];
            [self.textView.heightAnchor constraintEqualToConstant:140.0].active = true;
            [self.textView.widthAnchor constraintGreaterThanOrEqualToConstant:300.0].active = true;
            self.textView.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0] CGColor];
            self.textView.layer.borderWidth = 1.0f;
            self.textView.layer.cornerRadius = 5;
            [stackView addArrangedSubview:self.textView];
            
            AHButton *button = [AHButton buttonWithType:UIButtonTypeSystem];
            [button addTarget:self
                       action:@selector(textEntered:)
             forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:nextButtonText forState:UIControlStateNormal];
            [stackView addArrangedSubview:button];
            
        } else if ([questionTypeId isEqualToString:@"st"]) { // static text
            AHButton *button = [AHButton buttonWithType:UIButtonTypeSystem];
            [button addTarget:self
                       action:@selector(blankAnswer:)
             forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:nextButtonText forState:UIControlStateNormal];
            [stackView addArrangedSubview:button];

        } else  {
            NSLog(@"Error: invalid question type id found (%@)", questionTypeId);
        }
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

// [self buildChoice:i withTitle:[choices[i] valueForKeyPath:@"content"] withId:[choices[i] valueForKeyPath:@"id"] withOffset:neededSize.height + 70.0];
// - (void) buildChoice:(int)index withTitle:(NSString *)title withId:(NSString *) choiceId withOffset:(CGFloat) offset {}

- (void) makeChoice:(AHButton *)sender {
    //id userData = sender.userData;
    NSLog(@"makeChoice: %@", sender.userData);
    [self answerQuestion:sender.userData];
}

- (void) dateEntered:(AHButton *)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:[self.datePicker date]];
    
    NSLog(@"dateEntered: %@", dateString);
    [self answerQuestion:dateString];
}

- (void) textEntered:(AHButton *)sender {
    NSLog(@"textEntered: %@", [self.textView text]);
    [self answerQuestion:[self.textView text]];
}

- (void) blankAnswer:(AHButton *)sender {
    [self answerQuestion:@""];
}

- (void)removeSurveyResponse {
    NSString *authToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"authToken"];
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
    if (userId && userId.length > 0) {
        NSURL *baseURL = [NSURL URLWithString:baseURLString];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        [manager.requestSerializer setValue:@"application/form-data; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        NSString *urlString = [NSString stringWithFormat: @"surveys/%@/responses/%@", surveyId, responseId];
        [manager DELETE:urlString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"Survey: %@, response: %@ removed, responseObject: %@", surveyId, responseId, responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error: %@", error);
        }];
    } else {
        NSLog(@"Error: Not logged in");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
