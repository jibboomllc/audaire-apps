//
//  AHNumberUnitsView.m
//  Reader
//
//  Created by Tom Giesberg on 1/10/18.
//  Copyright © 2018 Audaire Health. All rights reserved.
//

#import "AHNumberUnitsView.h"

@implementation AHNumberUnitsView

- (AHNumberUnitsView *)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.axis = UILayoutConstraintAxisVertical;
        self.alignment = UIStackViewAlignmentCenter;
        
        numberLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        numberLabel.text = @"00";
        numberLabel.font = [UIFont systemFontOfSize:30];
        [self addArrangedSubview:numberLabel];

        unitsLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
        unitsLabel.text = @"-";
        unitsLabel.font = [UIFont systemFontOfSize:12];
        [self addArrangedSubview:unitsLabel];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

/*
- (void)layoutSubviews {
    [super layoutSubviews];
}
*/

- (void) setDisplayValues:(NSString *)numberString withUnits:(NSString *) unitsString {
    numberLabel.text = numberString;
    unitsLabel.text = unitsString;
    //NSLog(@"%@ %@", numberLabel.text, unitsLabel.text);

//    [self layoutSubviews];
}

-(void) scale:(CGFloat)scaleFactor {
    numberLabel.font = [UIFont systemFontOfSize:30 * scaleFactor];
    unitsLabel.font = [UIFont systemFontOfSize:12 * scaleFactor];
}

@end
