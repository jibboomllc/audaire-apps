//
//  LoginViewController.h
//  Reader
//
//  Created by Tom Giesberg on 8/15/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

@interface LoginViewController : UIViewController

-(IBAction) login:(id)sender;

@end
