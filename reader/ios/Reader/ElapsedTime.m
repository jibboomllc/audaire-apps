//
//  ElapsedTime.m
//  Reader
//
//  Created by Tom Giesberg on 2/6/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import "ElapsedTime.h"

@implementation ElapsedTime

@synthesize days;
@synthesize hours;
@synthesize minutes;
@synthesize seconds;

static ElapsedTime *instance = nil;

+(ElapsedTime *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [ElapsedTime new];
        }
    }
    return instance;
}


-(void) calculate:(NSDate *)since
{
    self.days = 0;
    self.hours = 0;
    self.minutes = 0;
    self.seconds = 0;

    if (since != nil) {
        long sPerDay = 60 * 60 * 24;
        long sPerHour = 60 * 60;
        long sPerMinute = 60;
        NSDate *now = [NSDate date];
        
        long dT = [now timeIntervalSinceDate:since];
        //NSLog(@"dT = %ld", dT);

        self.days = dT / sPerDay;
        dT -= self.days * sPerDay;
        
        self.hours = dT / sPerHour;
        dT -= self.hours * sPerHour;
        
        self.minutes = dT / sPerMinute;
        dT -= self.minutes * sPerMinute;
        
        self.seconds = dT;
    }
}

@end
