//
//  AppConstants.mm
//  Reader
//
//  Created by Tom Giesberg on 1/7/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppConstants.h"

#ifdef DEBUG

NSString *const baseURLString = @"https://audaire.io/api/v1/";

#else

NSString *const baseURLString = @"https://audaire.com/api/v1/";

#endif

UIColor *const accentColor = [UIColor colorWithRed:237.0/255.0 green:27.0/255.0 blue:46.0/255.0 alpha:1];
UIColor *const primaryColor = [UIColor colorWithRed:0.0/255.0 green:153.0/255.0 blue:51.0/255.0 alpha:1];
UIColor *const primaryDarkColor = [UIColor colorWithRed:0.0/255.0 green:135.0/255.0 blue:45.0/255.0 alpha:1];

