//
//  AHElapsedTimeView.m
//  Reader
//
//  Created by Tom Giesberg on 1/10/18.
//  Copyright © 2018 Audaire Health. All rights reserved.
//

#import "AHElapsedTimeView.h"
#import "AHNumberUnitsView.h"

@implementation AHElapsedTimeView

- (AHElapsedTimeView *)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.axis = UILayoutConstraintAxisVertical;
        self.alignment = UIStackViewAlignmentCenter;
        self.spacing = 10;
        daysView = [[AHNumberUnitsView alloc]init];
        [daysView scale:2.0];
        [self addArrangedSubview:daysView];
        
        UIStackView *stackView = [[UIStackView alloc] init];
        //stackView.axis = UILayoutConstraintAxisVertical;
        //stackView.distribution = UIStackViewDistributionEqualSpacing;
        //stackView.alignment = UIStackViewAlignmentCenter;
        stackView.spacing = 10;
        //stackView.layoutMargins = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
        [self addArrangedSubview:stackView];

        hoursView = [[AHNumberUnitsView alloc]init];
        [stackView addArrangedSubview:hoursView];
        
        UIView *divider = [[UIView alloc] init];
        divider.backgroundColor = [UIColor darkGrayColor];
        [divider.heightAnchor constraintEqualToConstant:48].active = true;
        [divider.widthAnchor constraintEqualToConstant:1].active = true;
        [stackView addArrangedSubview:divider];

        minutesView = [[AHNumberUnitsView alloc]init];
        [stackView addArrangedSubview:minutesView];
        
        UIView *divider2 = [[UIView alloc] init];
        divider2.backgroundColor = [UIColor darkGrayColor];
        [divider2.heightAnchor constraintEqualToConstant:48].active = true;
        [divider2.widthAnchor constraintEqualToConstant:1].active = true;
        [stackView addArrangedSubview:divider2];

        secondsView = [[AHNumberUnitsView alloc]init];
        [stackView addArrangedSubview:secondsView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

/*
- (void)layoutSubviews {
    [super layoutSubviews];
}
*/

- (void)setElapsedTime:(ElapsedTime *)elapsedTime {
    [daysView setDisplayValues:[NSString stringWithFormat:@"%ld", elapsedTime.days]
                        withUnits:[NSString stringWithFormat:@"Day%@", (elapsedTime.days != 1 ? @"s" : @"")]];
    [hoursView setDisplayValues:[NSString stringWithFormat:@"%02ld", elapsedTime.hours]
                        withUnits:[NSString stringWithFormat:@"Hour%@", (elapsedTime.hours != 1 ? @"s" : @"")]];
    [minutesView setDisplayValues:[NSString stringWithFormat:@"%02ld", elapsedTime.minutes]
                        withUnits:[NSString stringWithFormat:@"Minute%@", (elapsedTime.minutes != 1 ? @"s" : @"")]];
    [secondsView setDisplayValues:[NSString stringWithFormat:@"%02ld", elapsedTime.seconds]
                        withUnits:[NSString stringWithFormat:@"Second%@", (elapsedTime.seconds != 1 ? @"s" : @"")]];
    [self layoutSubviews];
}

@end
