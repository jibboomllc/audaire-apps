//
//  SettingsViewController.h
//  Reader
//
//  Created by Tom Giesberg on 12/20/16.
//  Copyright © 2016 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController: UITableViewController

-(IBAction) logout:(id)sender;

@end
