//
//  AppConstants.h
//  Reader
//
//  Created by Tom Giesberg on 1/7/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#ifndef AppConstants_h
#define AppConstants_h

extern NSString* const baseURLString;

extern UIColor* const accentColor;
extern UIColor* const primaryColor;
extern UIColor* const primaryDarkColor;


#endif /* AppConstants_h */
