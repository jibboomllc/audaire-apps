//
//  AHButton.m
//  Reader
//
//  Created by Tom Giesberg on 12/29/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "AHButton.h"

@implementation AHButton

@synthesize userData;

+ (instancetype)buttonWithType:(UIButtonType)buttonType {
    AHButton *button = [super buttonWithType:buttonType];
    [button postButtonWithTypeInit];
    [button setExclusiveTouch:NO];
    button.layer.borderColor = [[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0] CGColor];
    button.layer.borderWidth = 1.0f;
    button.layer.cornerRadius = 5;
    [button.heightAnchor constraintEqualToConstant:40.0].active = true;
    [button.widthAnchor constraintGreaterThanOrEqualToConstant:240.0].active = true;
    return button;
}

/// Because we can't override init on a uibutton, do init steps here.
- (void)postButtonWithTypeInit {
    //[self setTitle...
}
// Lays out the image on right side of button
- (void)XXlayoutSubviews {
    [super layoutSubviews];
    if (self.imageView != nil) {
        self.imageEdgeInsets = UIEdgeInsetsMake(5, (self.bounds.size.width - 35), 5, 5);
        self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, self.imageView.frame.size.width);
    }
}

@end
