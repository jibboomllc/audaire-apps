//
//  TabBarViewController.m
//  Reader
//
//  Created by Tom Giesberg on 1/7/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import "TabBarViewController.h"
#import "AppConstants.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [[UITabBar appearance] setBarTintColor:primaryColor];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
 
    if ([UITabBar instancesRespondToSelector:@selector(setUnselectedItemTintColor:)]) {
        // for iOS >= 10
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor whiteColor]];
    } else {
        // fo iOS < 10
        self.tabBar.tintColor = [UIColor whiteColor];
        //[[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];

        UITabBarItem *item = [self.tabBar.items objectAtIndex:0];
        
        // this way, the icon gets rendered as it is (thus, it needs to be green in this example)
        item.image = [[UIImage imageNamed:@"Home"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        // this icon is used for selected tab and it will get tinted as defined in self.tabBar.tintColor
        //item.selectedImage = [UIImage imageNamed:@"Home Filled"];
        
        item = [self.tabBar.items objectAtIndex:1];
        item.image = [[UIImage imageNamed:@"Scan"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        //item.selectedImage = [UIImage imageNamed:@"Status Filled"];
        
        item = [self.tabBar.items objectAtIndex:2];
        item.image = [[UIImage imageNamed:@"Settings"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        //item.selectedImage = [UIImage imageNamed:@"Settings Filled"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
