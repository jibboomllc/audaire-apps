//
//  HomeViewController.m
//  Reader
//
//  Created by Tom Giesberg on 12/19/16.
//  Copyright © 2016 Audaire Health. All rights reserved.
//


#import <AFNetworking/AFNetworking.h>
#import <AVFoundation/AVFoundation.h>

#import "HomeViewController.h"
#import "ScanViewController.h"
#import "SurveyViewController.h"
#import "AppConstants.h"
#import "AHButton.h"

@interface HomeViewController ()

@property (nonatomic, strong) AVAudioPlayer *successSound;

- (void)loadSounds;
- (void)useBarcodeScanNotification:(NSNotification*)notification;

@end

static NSDateFormatter *sPostgreSQLDateFormatter;

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if (@available(iOS 10, *)) {
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
        [refreshControl addTarget:self
                           action:@selector(refreshData:)
                 forControlEvents:UIControlEventValueChanged];
        
        scrollView.refreshControl = refreshControl;
    }

    // build stack view
    stackView = [[UIStackView alloc] init];
    stackView.axis = UILayoutConstraintAxisVertical;
    stackView.distribution = UIStackViewDistributionEqualSpacing;
    stackView.alignment = UIStackViewAlignmentCenter;
    stackView.spacing = 30;
    stackView.layoutMargins = UIEdgeInsetsMake(50.0, 10.0, 10.0, 10.0);
    stackView.layoutMarginsRelativeArrangement = true;
    stackView.translatesAutoresizingMaskIntoConstraints = false;
    [scrollView addSubview:stackView];
    
    [stackView.leadingAnchor constraintEqualToAnchor:scrollView.leadingAnchor].active = true;
    [stackView.trailingAnchor constraintEqualToAnchor:scrollView.trailingAnchor].active = true;
    [stackView.topAnchor constraintEqualToAnchor:scrollView.topAnchor].active = true;
    [stackView.bottomAnchor constraintEqualToAnchor:scrollView.bottomAnchor].active = true;
    [stackView.widthAnchor constraintEqualToAnchor:scrollView.widthAnchor].active = true;
    
    elapsedTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
    elapsedTimeLabel.text = @"since last usage";
    elapsedTimeLabel.font = [UIFont systemFontOfSize:20];

    elapsedTimeView = [[AHElapsedTimeView alloc] init];
    scanBarcodeButton = [AHButton buttonWithType:UIButtonTypeSystem];
    [scanBarcodeButton addTarget:self
               action:@selector(showBarCodeScanner:)
     forControlEvents:UIControlEventTouchUpInside];
    //[scanBarcodeButton setTitle:@"Scan label" forState:UIControlStateNormal];
    [scanBarcodeButton setImage:[UIImage imageNamed:@("Barcode Filled")] forState:UIControlStateNormal];
    scanBarcodeButton.layer.borderWidth = 0;

    // If the date formatters aren't already set up, do that now and cache them
    // for subsequence reuse.
    if (sPostgreSQLDateFormatter == nil) {
        NSLocale *enUSPOSIXLocale;
        sPostgreSQLDateFormatter = [[NSDateFormatter alloc] init];
        enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        [sPostgreSQLDateFormatter setLocale:enUSPOSIXLocale];
        // 2016-06-12 18:36:37.911467-04
        [sPostgreSQLDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSSZ"];
        [sPostgreSQLDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }

    [self loadSounds];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(useBarcodeScanNotification:)
     name:@"BarcodeScan"
     object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [self refreshData:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)useBarcodeScanNotification:(NSNotification *)notification {
    NSString *key = @"Barcode";
    NSDictionary *dictionary = [notification userInfo];
    NSString *barcode = [dictionary valueForKey:key];

    [self registerTagDisposal: barcode];
}

- (IBAction)cancelToHome:(UIStoryboardSegue *) segue {
}

- (void) refreshData:(UIRefreshControl *)sender {
    // clean up any old views
    [[stackView arrangedSubviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    [stackView addArrangedSubview:elapsedTimeView];
    [stackView addArrangedSubview:elapsedTimeLabel];
    [stackView addArrangedSubview:scanBarcodeButton];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    // Lookup the last disposal
    NSString *authToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"authToken"];
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
    if (userId && userId.length > 0) {

        NSString *url = [NSString stringWithFormat: @"%@usages/patient_id/%@/last", baseURLString, userId];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
             if (responseObject != Nil) {
                 NSLog(@"JSON: %@", responseObject);
                 ElapsedTime *elapsedTime = [[ElapsedTime alloc]init];
                 [elapsedTime calculate:[sPostgreSQLDateFormatter dateFromString:[responseObject valueForKeyPath:@"disposal_time"]]];
                 elapsedTimeView.elapsedTime = elapsedTime;
             }
            [scrollView.refreshControl endRefreshing];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [scrollView.refreshControl endRefreshing];
        }];
    }
    
    [self getSurveys];
}

- (void)registerTagDisposal:(NSString *) scannedCode {
    NSString *authToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"authToken"];
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
    if (userId && userId.length > 0) {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [manager.requestSerializer setValue:@"application/form-data; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        NSString *url = [NSString stringWithFormat: @"%@usages/verbose", baseURLString];
        
        float scaleFactor = [[UIScreen mainScreen] scale];
        CGRect screen = [[UIScreen mainScreen] bounds];
        CGFloat widthInPixel = screen.size.width * scaleFactor;
        CGFloat heightInPixel = screen.size.height * scaleFactor;
        
        NSDictionary *disposalDataDictionary = @{
                                               @"type": @"barcode"
                                               , @"platform": @"ios"
                                               , @"os_version": [[UIDevice currentDevice] systemVersion]
                                               , @"os_locale": [NSLocale currentLocale].localeIdentifier
                                               , @"app_version": [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]
                                               , @"display_x": [NSString stringWithFormat: @"%.0f", widthInPixel]
                                               , @"display_y": [NSString stringWithFormat: @"%.0f", heightInPixel]
                                               //, @"latitude": @"", @"longitude": @"", @"device_id": @""
                                               };
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:disposalDataDictionary options:0 error:&error];
        NSString *jsonString;
        if (!jsonData) {
            NSLog(@"Got an error: %@", error);
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        NSDictionary *parametersDictionary = @{
                                               @"disposal_data": jsonString
                                               , @"tag_ids": [NSString stringWithFormat: @"[\"%@\"]", scannedCode]
                                               };
        [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (responseObject != Nil) {
                //NSLog(@"responseObject: %@", responseObject);
                int readsSaved = [[responseObject valueForKeyPath:@"reads_saved"] intValue];
                NSDictionary *details = [[responseObject valueForKeyPath:@"details"] objectAtIndex:0];
                NSString *result = [details valueForKeyPath:@"result"];
                NSString *tagID = [details valueForKeyPath:@"tag_id"];
                if (_successSound && readsSaved > 0) {
                    [_successSound play];
                }
                NSString *title;
                NSString *message;
                if ([result isEqualToString:@"OK"]) {
                    title = [NSString stringWithFormat:@"Scan Success!"];
                    message = [NSString stringWithFormat:@"Successfully scanned label: %@", tagID];
                } else if ([result isEqualToString:@"DEMO"]) {
                    title = [NSString stringWithFormat:@"Scan Success!"];
                    message = [NSString stringWithFormat:@"Successfully scanned demo label: %@", tagID];
                } else if ([result isEqualToString:@"BAD"]) {
                    title = [NSString stringWithFormat:@"Scan Error!"];
                    message = [NSString stringWithFormat:@"Code: %@ not recognized. Please make sure that you are scanning the code on the small Audaire label.", tagID];
                } else if ([result isEqualToString:@"USED"]) {
                    title = [NSString stringWithFormat:@"Scan Error!"];
                    message = [NSString stringWithFormat:@"Rescanned label: %@. Originally scanned: %@", tagID, [details valueForKeyPath:@"disposal_time"]];
                } else if ([result isEqualToString:@"UNREG"]) {
                    title = [NSString stringWithFormat:@"Scan Error!"];
                    message = [NSString stringWithFormat:@"Label: %@ does not appear to be registered with Audaire", tagID];
                } else if ([result isEqualToString:@"ERROR"]) {
                    title = [NSString stringWithFormat:@"Scan Error!"];
                    message = [NSString stringWithFormat:@"Error saving label: %@", tagID];
                } else {
                    title = [NSString stringWithFormat:@"Scan Error!"];
                    message = [NSString stringWithFormat:@"Unknown status: %@", result];
                }
#if (TARGET_IPHONE_SIMULATOR)
                UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [messageAlert show];
#else
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:title
                                             message:message
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"OK"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               //Handle OK button
                                               //NSLog(@"OK pushed");
                                           }];
                
                [alert addAction:okButton];
                
                [self presentViewController:alert animated:YES completion:nil];
#endif
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error: %@", error);
        }];
    } else {
        //        lastDisposal.text = @"Please set serial number";
    }
    return;
}

-(void)loadSounds{
    NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"read" ofType:@"mp3"];
    NSURL *beepURL = [NSURL URLWithString:beepFilePath];
    NSError *error;
    
    beepFilePath = [[NSBundle mainBundle] pathForResource:@"success" ofType:@"mp3"];
    beepURL = [NSURL URLWithString:beepFilePath];
    _successSound = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
    if (error) {
        NSLog(@"Could not open success file.");
        NSLog(@"%@", [error localizedDescription]);
    }
    else{
        [_successSound prepareToPlay];
    }
}

- (void)getSurveys {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *authToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"authToken"];
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
    if (userId && userId.length > 0) {
        NSString *url = [NSString stringWithFormat: @"%@surveys/patient_id/%@", baseURLString, userId];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            if (responseObject != Nil) {
                surveysObject = responseObject;
                [self buildSurveyList];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    } else {
        NSLog(@"Error: Not logged in");
    }
}

- (void)buildSurveyList {
    NSMutableArray *surveys = [surveysObject mutableArrayValueForKey:@"surveys"];
    for (int i = 0; i < [surveys count]; i++) {
        AHButton *button = [AHButton buttonWithType:UIButtonTypeSystem];
        [button addTarget:self
                   action:@selector(showSurvey:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:[surveys[i] valueForKeyPath:@"name"] forState:UIControlStateNormal];
        [button setUserData:[surveys[i] valueForKeyPath:@"id"]];
        [stackView addArrangedSubview:button];
    }
}

- (void) showSurvey:(AHButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SurveyViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"SurveyViewController"];
    [viewController setSurveyTitle:sender.currentTitle];
    [viewController setSurveyId: sender.userData];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void) showBarCodeScanner:(AHButton *)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SurveyViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ScanViewController"];
    [self presentViewController:viewController animated:YES completion:nil];
}

@end
