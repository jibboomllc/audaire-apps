//
//  AHButton.h
//  Reader
//
//  Created by Tom Giesberg on 12/29/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AHButton : UIButton {
    id userData;
}

@property (nonatomic, readwrite, retain) id userData;

@end