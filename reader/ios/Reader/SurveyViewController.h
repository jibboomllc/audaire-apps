//
//  SurveyViewController.h
//  Reader
//
//  Created by Tom Giesberg on 12/30/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SurveyViewController : UIViewController {
    IBOutlet UINavigationBar *navBar;
    IBOutlet UIScrollView *scrollView;
 }

@property (nonatomic, readwrite, retain) NSString *surveyId;
@property (nonatomic, readwrite, retain) NSString *surveyTitle;
@property (nonatomic, readwrite, retain) NSString *responseId;
@property (nonatomic, readwrite, retain) id questionObject;
@property (nonatomic, readwrite, retain) UITextView *textView;
@property (nonatomic, readwrite, retain) UIDatePicker *datePicker;

@end

