//
//  UIColor+Hexadecimal.h
//  Reader
//
//  Created by Tom Giesberg on 9/24/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor(Hexadecimal)

+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
