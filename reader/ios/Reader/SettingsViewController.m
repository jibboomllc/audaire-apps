//
//  SettingsViewController.m
//  Reader
//
//  Created by Tom Giesberg on 12/20/16.
//  Copyright © 2016 Audaire Health. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

#import "SettingsViewController.h"
#import "AppDelegate.h"
#import "AppConstants.h"

@interface SettingsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *vnText;
@property (weak, nonatomic) IBOutlet UILabel *bnText;

@property (weak, nonatomic) IBOutlet UILabel *userNameText;
@property (weak, nonatomic) IBOutlet UISwitch *userLogInOut;

@property (weak, nonatomic) IBOutlet UILabel *snText;
@property (weak, nonatomic) IBOutlet UILabel *elapsedLastPing;

- (IBAction)refresh:(UIRefreshControl *)sender;
@end

static NSDateFormatter *    sPostgreSQLDateFormatter;

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSString *firstName = [[NSUserDefaults standardUserDefaults] stringForKey:@"firstName"];
    NSString *lastName = [[NSUserDefaults standardUserDefaults] stringForKey:@"lastName"];
    
    _userNameText.text = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    
    _vnText.text = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    _bnText.text = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    
    // If the date formatters aren't already set up, do that now and cache them
    // for subsequence reuse.
    
    if (sPostgreSQLDateFormatter == nil) {
        NSLocale *                  enUSPOSIXLocale;
        
        sPostgreSQLDateFormatter = [[NSDateFormatter alloc] init];
        
        enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        
        [sPostgreSQLDateFormatter setLocale:enUSPOSIXLocale];
        // 2016-06-12 18:36:37.911467-04
        [sPostgreSQLDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSSZ"];
        
        [sPostgreSQLDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self refresh: nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelToSettings:(UIStoryboardSegue *) segue {
}

- (IBAction)refresh:(UIRefreshControl *)sender {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSString *authToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"authToken"];
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
    
    // Lookup the last ping
    if (userId && userId.length > 0) {
        NSString *url = [NSString stringWithFormat: @"%@ping/patient_id/%@", baseURLString, userId];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [self.elapsedLastPing setText:@""];
            [self.snText setText:@""];
            
            NSLog(@"JSON: %@", responseObject);
            if (responseObject != Nil) {
                if ([[responseObject valueForKeyPath:@"reader_found"]longValue] == 1) {
                    int minutesSinceLastPing = [self minutesSince:[responseObject valueForKeyPath:@"data.last_ping"]];
                    if (minutesSinceLastPing > 60) {
                        [self.elapsedLastPing setText:@"60+ min"];
                    } else {
                        [self.elapsedLastPing setText:[NSString stringWithFormat:@"%d min", minutesSinceLastPing]];
                    }
                    [self.snText setText:[responseObject valueForKeyPath:@"data.serial_number"]];
                }
            }
            if (sender) {
                [sender endRefreshing];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            if (sender) {
                [sender endRefreshing];
            }
        }];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    } else if (section == 1) {
        return 2;
    } else if (section == 2) {
        return 2;
    }
    return 0;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://audaire.com/privacy_policy.html"]];
        } else if (indexPath.row == 1) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://audaire.com/terms_of_service.html"]];
        }
    }
}

- (IBAction)logout:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate logout];
}

- (int)minutesSince:(NSString *)postgreSQLDateTimeString
{
    NSDate *date = [sPostgreSQLDateFormatter dateFromString:postgreSQLDateTimeString];
    NSDate *now = [NSDate date];
    NSTimeInterval distanceBetweenDates = [now timeIntervalSinceDate:date];
    return distanceBetweenDates / 60;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
