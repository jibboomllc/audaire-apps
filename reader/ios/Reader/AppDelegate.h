//
//  AppDelegate.h
//  Reader
//
//  Created by Tom Giesberg on 12/19/16.
//  Copyright © 2016 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)logout;


@end

