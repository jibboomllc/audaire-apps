//
//  LoginViewController.m
//  Reader
//
//  Created by Tom Giesberg on 8/15/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

#import "AppConstants.h"
#import "LoginViewController.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender {
    NSString *email = _emailTextField.text;
    NSString *password = _passwordTextField.text;
    
    if (email.length > 0 && password.length > 0) {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [manager.requestSerializer setValue:@"application/form-data; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        NSString *url = [NSString stringWithFormat: @"%@login", baseURLString];
        NSDictionary *parametersDictionary = @{@"email": email, @"password": password};
        [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (responseObject != Nil) {
                NSString *authToken = [responseObject valueForKeyPath:@"token"];
                NSString *userId = [responseObject valueForKeyPath:@"id"];
                
                if (authToken != Nil && authToken.length > 0 && userId != Nil && userId.length > 0) {
                    // Save details
                    NSString *firstName = [responseObject valueForKeyPath:@"first_name"];
                    NSString *lastName = [responseObject valueForKeyPath:@"last_name"];

                    [[NSUserDefaults standardUserDefaults] setObject:authToken forKey:@"authToken"];
                    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"userId"];
                    [[NSUserDefaults standardUserDefaults] setObject:firstName forKey:@"firstName"];
                    [[NSUserDefaults standardUserDefaults] setObject:lastName forKey:@"lastName"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
     
                    // Send notification
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccessful" object:self];
                    
                    // Dismiss login screen
                    [self dismissViewControllerAnimated:YES completion:nil];
                } else {
                    NSLog(@"Email and/or password not found");
                    _errorLabel.text = @"Email and/or password not found";
                }
            } else {
                NSLog(@"Unknown error trying to login");
                _errorLabel.text = @"Unknown error trying to login";
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error: %@", error);
            _errorLabel.text = @"Session error";
        }];
    } else {
        NSLog(@"Email and password required");
        _errorLabel.text = @"Email and password required";
    }
}

@end

