//
//  AHRoundedLabel.h
//  Reader
//
//  Created by Tom Giesberg on 1/9/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface AHRoundedLabel : UILabel

@end
