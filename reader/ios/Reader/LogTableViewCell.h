//
//  LogTableViewCell.h
//  Reader
//
//  Created by Tom Giesberg on 8/18/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *summaryLabel;
@property (nonatomic, weak) IBOutlet UILabel *markerLabel;

@end

