//
//  AHRoundedLabel.m
//  Reader
//
//  Created by Tom Giesberg on 1/9/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import "AHRoundedLabel.h"

@implementation AHRoundedLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSubviews {
    [super layoutSubviews];
    self.layer.cornerRadius = 0.5 * self.bounds.size.height;
    self.clipsToBounds = true;
}

@end
