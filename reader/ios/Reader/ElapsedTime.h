//
//  ElapsedTime.h
//  Reader
//
//  Created by Tom Giesberg on 2/6/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ElapsedTime : NSObject {
    long days;
    long hours;
    long minutes;
    long seconds;
}

@property(nonatomic)long days;
@property(nonatomic)long hours;
@property(nonatomic)long minutes;
@property(nonatomic)long seconds;

+(ElapsedTime *)getInstance;

-(void) calculate:(NSDate *)since;

@end

