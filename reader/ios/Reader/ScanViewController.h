//
//  ScanViewController.h
//  Reader
//
//  Created by Tom Giesberg on 12/19/16.
//  Copyright © 2016 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScanViewController : UIViewController {
    IBOutlet UIView *viewPreview;
}

@end

