//
//  LogViewController.m
//  Reader
//
//  Created by Tom Giesberg on 8/18/17.
//  Copyright © 2017 Audaire Health. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

#import "LogTableViewCell.h"
#import "AppConstants.h"
#import "LogViewController.h"
#import "UIColor+Hexadecimal.h"

@interface LogViewController ()
- (IBAction)refresh:(UIRefreshControl *)sender;

@property (strong,nonatomic) NSArray *content;

@end

@implementation LogViewController

static NSDateFormatter *    sUserVisibleDateFormatter;
static NSDateFormatter *    sPostgreSQLDateFormatter;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // If the date formatters aren't already set up, do that now and cache them
    // for subsequent reuse.
    
    if (sPostgreSQLDateFormatter == nil) {
        NSLocale *enUSPOSIXLocale;
        sPostgreSQLDateFormatter = [[NSDateFormatter alloc] init];
        enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        [sPostgreSQLDateFormatter setLocale:enUSPOSIXLocale];
        [sPostgreSQLDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        [sPostgreSQLDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    
    if (sUserVisibleDateFormatter == nil) {
        sUserVisibleDateFormatter = [[NSDateFormatter alloc] init];
        [sUserVisibleDateFormatter setDateStyle:NSDateFormatterShortStyle];
        [sUserVisibleDateFormatter setTimeStyle:NSDateFormatterShortStyle];
    }

    [self refresh: nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _content.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    LogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"logCell" forIndexPath:indexPath];
    NSDictionary *event = [_content objectAtIndex:indexPath.row];
    cell.markerLabel.text = [[event valueForKey:@"title"] substringToIndex:1];
    cell.markerLabel.textColor = [UIColor colorWithHexString: @"ffffff"];
    if ([[event valueForKey:@"class"] isEqualToString:@"event-important"]) {
        cell.markerLabel.backgroundColor = [UIColor colorWithHexString: @"ad2121"];
    } else if ([[event valueForKey:@"class"] isEqualToString:@"event-info"]) {
        cell.markerLabel.backgroundColor = [UIColor colorWithHexString: @"1e90ff"];
    } else if ([[event valueForKey:@"class"] isEqualToString:@"event-warning"]) {
        cell.markerLabel.backgroundColor = [UIColor colorWithHexString: @"e3bc08"];
    } else if ([[event valueForKey:@"class"] isEqualToString:@"event-inverse"]) {
        cell.markerLabel.backgroundColor = [UIColor colorWithHexString: @"1b1b1b"];
    } else if ([[event valueForKey:@"class"] isEqualToString:@"event-success"]) {
        cell.markerLabel.backgroundColor = [UIColor colorWithHexString: @"006400"];
    } else if ([[event valueForKey:@"class"] isEqualToString:@"event-special"]) {
        cell.markerLabel.backgroundColor = [UIColor colorWithHexString: @"800080"];
    }
    cell.titleLabel.text = [event valueForKey:@"title"];
    cell.summaryLabel.text = [self userVisibleDateTimeString:[event valueForKey:@"start"]];
    NSDate *date = [[NSDate alloc]initWithTimeIntervalSince1970:(NSTimeInterval)[[event valueForKey:@"start"]doubleValue]/1000];
    cell.summaryLabel.text = [sUserVisibleDateFormatter stringFromDate:date];
    
    // do event-specific formatting
    NSData *jsonData = [[event valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSDictionary *metaData = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
    if ([[event valueForKey:@"type"] isEqualToString: @"usage"]) {
        cell.markerLabel.textColor = [UIColor colorWithHexString: [metaData valueForKey:@"font_color"]];
        
        if ([metaData valueForKey:@"background_color"] != [NSNull null]) {
            cell.markerLabel.backgroundColor = [UIColor colorWithHexString: [metaData valueForKey:@"background_color"]];
            if ([[metaData valueForKey:@"background_color"] isEqualToString:(@"ffffff")]) {
                cell.markerLabel.layer.borderColor = [[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0] CGColor];
                cell.markerLabel.layer.borderWidth = 1.0f;
            }
        }
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *event = [_content objectAtIndex:indexPath.row];
    // NSDate *date = [[NSDate alloc]initWithTimeIntervalSince1970:(NSTimeInterval)[[event valueForKey:@"start"]doubleValue]/1000];
    // NSString *title = [NSString stringWithFormat: @"%@ Event", [sUserVisibleDateFormatter stringFromDate:date]];
    NSString *message;
    NSData *jsonData = [[event valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSDictionary *metaData = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
    if ([[event valueForKey:@"type"] isEqualToString: @"usage"]) {
        message = [NSString stringWithFormat: @"%@\nLabel: %@\nMethod: %@", [event valueForKey:@"title"], [metaData valueForKey:@"tag_id"], [metaData valueForKeyPath:@"disposal_data.type"]];
    } else {
        message = [NSString stringWithFormat: @"%@", [event valueForKey:@"title"]];
    }
    
#if (TARGET_IPHONE_SIMULATOR)
    UIAlertView *messageAlert = [[UIAlertView alloc] initWithTitle:@"Event" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [messageAlert show];
#else
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Event"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle OK button
                                   //NSLog(@"OK pushed");
                               }];
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
#endif
    
}
- (IBAction)refresh:(UIRefreshControl *)sender {
    // Lookup the last disposals
    NSString *authToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"authToken"];
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];
    if (userId && userId.length > 0) {
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        NSString *fromTimestamp = [NSString stringWithFormat:@"%f",([[NSDate date] timeIntervalSince1970] - (90 * 24 * 60 * 60)) * 1000];
        NSString *toTimestamp = [NSString stringWithFormat:@"%f",([[NSDate date] timeIntervalSince1970] + (24 * 60 * 60)) * 1000];
        NSString *url = [NSString stringWithFormat: @"%@events/patient_id/%@?&from=%@&to=%@", baseURLString, userId, fromTimestamp, toTimestamp];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            if (responseObject != Nil) {
                //NSLog(@"JSON: %@", responseObject);
                NSArray *events = [responseObject valueForKeyPath:@"result"];
                self.content = events;
                [self.tableView reloadData];
                //for (NSString *event in events) {NSLog(@"JSON: %@", event);}
            }
            if (sender) {
                [sender endRefreshing];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            if (sender) {
                [sender endRefreshing];
            }
        }];
    }
}

- (NSString *)userVisibleDateTimeString:(NSString *)postgreSQLDateTimeString
{
    NSString *userVisibleDateTimeString;
    NSDate *date;
    
    // Convert the PostgreSQL date time string to an NSDate.
    // Then convert the NSDate to a user-visible date string.
    
    userVisibleDateTimeString = nil;
    
    date = [sPostgreSQLDateFormatter dateFromString:postgreSQLDateTimeString];
    //date = [sPostgreSQLDateFormatter dateFromString:@"2016-06-12 18:36:37.911467-04"];
    
    if (date != nil) {
        userVisibleDateTimeString = [sUserVisibleDateFormatter stringFromDate:date];
    }
    return userVisibleDateTimeString;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
