//
//  AHElapsedTimeView.h
//  Reader
//
//  Created by Tom Giesberg on 1/10/18.
//  Copyright © 2018 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ElapsedTime.h"
#import "AHNumberUnitsView.h"

@interface AHElapsedTimeView : UIStackView {
    AHNumberUnitsView *daysView;
    AHNumberUnitsView *hoursView;
    AHNumberUnitsView *minutesView;
    AHNumberUnitsView *secondsView;
}

-(void)setElapsedTime:(ElapsedTime *)elapsedTime;

@end
