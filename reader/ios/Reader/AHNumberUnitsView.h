//
//  AHNumberUnitsView.h
//  Reader
//
//  Created by Tom Giesberg on 1/10/18.
//  Copyright © 2018 Audaire Health. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ElapsedTime.h"

@interface AHNumberUnitsView : UIStackView {
    UILabel *numberLabel;
    UILabel *unitsLabel;
}

- (void) setDisplayValues:(NSString *)numberString withUnits:(NSString *) unitsString;
- (void) scale:(CGFloat)scaleFactor;

@end
